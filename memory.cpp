#include "memory.h"


double *allocate_1d_double_array (int n){
	double *array = new double[n];
	return array;
}	
//---------------------------------------------------------------------

double **allocate_2d_double_array (int rows,int cols){

	double **array = new double *[rows];
	for (int i = 0; i < rows; i++)
  	array[i] = new double [cols];
	return array;
}
//---------------------------------------------------------------------

double ***allocate_3d_double_array (int rows,int cols,int dir){

	double ***array = new double **[rows];
	for (int i = 0; i < rows; i++){
  	array[i] = new double *[cols];
  	for (int j = 0; j < cols; j++)
  		array[i][j] = new double [dir];
  }	
	return array;
}
//---------------------------------------------------------------------

int *allocate_1d_int_array (int n){
	int *array = new int[n];
	return array;
}
//---------------------------------------------------------------------

int **allocate_2d_int_array (int rows,int cols){

	int **array = new int *[rows];
	for (int i = 0; i < rows; i++)
  	array[i] = new int [cols];
	return array;
}
//---------------------------------------------------------------------

int ***allocate_3d_int_array (int rows,int cols,int dir){

	int ***array = new int **[rows];
	for (int i = 0; i < rows; i++){
  	array[i] = new int *[cols];
  	for (int j = 0; j < cols; j++)
  		array[i][j] = new int [dir];
  }	
	return array;
}
//---------------------------------------------------------------------
int **allocate_2d_int_variable_array (int rows,int *cols){

	int **array = new int *[rows];
	for (int i = 0; i < rows; i++)
  	array[i] = new int [cols[i]];
	return array;
}
//---------------------------------------------------------------------

void free_1d_double_array (double *array){
	delete[] array;
}
//---------------------------------------------------------------------

void free_2d_double_array (double **array, int rows){

	for (int i = 0; i < rows; i++) 
  	delete[] array[i];
	delete[] array;
}
//---------------------------------------------------------------------

void free_3d_double_array (double ***array, int rows,int cols){

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++)
  		delete[] array[i][j];
  	delete[] array[i];
  }
	delete[] array;
}
//---------------------------------------------------------------------

void free_1d_int_array (int *array){
	delete[] array;
}
//---------------------------------------------------------------------

void free_2d_int_array (int **array, int rows){

	for (int i = 0; i < rows; i++) 
  	delete[] array[i];
	delete[] array;
}
//---------------------------------------------------------------------

void free_3d_int_array (int ***array, int rows,int cols){

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++)
  		delete[] array[i][j];
  	delete[] array[i];
  }
	delete[] array;
}
//---------------------------------------------------------------------
